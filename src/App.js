import "./App.css";
import { useEffect, useState } from "react";
import { Routes, Route, Link, useParams } from "react-router-dom";

function Books() {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    fetch("https://adsd-bookstore-api.herokuapp.com/books")
      .then((res) => res.json())
      .then((data) => setBooks(data))
      .catch((err) => console.error(err));
  }, []);

  return (
    <div>
      <Routes>
        <Route path="/books" element={<Books />} />
        <Route path="/book/:id" element={<Book />} />
      </Routes>
      <p>Mijn boeken</p>
      <ul>
        {books.map((book, index) => {
          const link = "/books/" + index;
          return (
            <Link to={link}>
              <li key={index}>
                {book.author.name} - {book.title}
              </li>
            </Link>
          );
        })}
      </ul>
    </div>
  );
}

function Book() {
  const [book, setBook] = useState({});
  const params = useParams();

  useEffect(() => {
    fetch("https://java-web-api.herokuapp.com/books/" + params.id)
      .then((res) => res.json())
      .then((data) => setBook(data))
      .catch((err) => console.error(err));
  }, []);

  return (
    <div>
      <b>Book:</b>
      <p>{book?.title}</p>
      <b>author:</b>
      <p>
        {book?.author?.name}
        {book?.author?.country}
      </p>
      <b>Genre:</b>
      {book?.genre}
      <Link to="/books">Back to books</Link>
    </div>
  );
}

function App() {
  return (
    <div>
      <Routes>
        <Route path="/books" element={<Books />} />
        <Route path="/books/:id" element={<Book />} />
      </Routes>
    </div>
  );
}

export default App;
